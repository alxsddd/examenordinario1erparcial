/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examenordinario;

/**
 *
 * @author Alexis
 */
public class nomina {
    private int numRecibo ;
    private String nombre;
    private int puesto;
    private int nivel;
    private int diasTrab;


public nomina(){
        this.numRecibo = 23;
        this.nombre = "Jose Lopez Acosta";
        this.puesto = 2;
        this.nivel = 1;
        this.diasTrab = 10;

    }    
public nomina(int numRecibo, String nombre, int puesto, int nivel, int diasTrab) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.diasTrab = diasTrab;
    }
 public nomina(nomina otro) {
        this.numRecibo = otro.numRecibo;
        this.nombre = otro.nombre;
        this.puesto = otro.puesto;
        this.nivel = otro.nivel;
        this.diasTrab = otro.diasTrab;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDiasTrab() {
        return diasTrab;
    }

    public void setDiasTrab(int diasTrab) {
        this.diasTrab = diasTrab;
    }
 
 public float calcularPago() {
        float pago = 0.0f;
        if (puesto == 1) {
            pago = this.diasTrab * 100f;
        }
        if (puesto == 2) {
            pago = this.diasTrab * 200f;
        }
        if (puesto == 3) {
            pago = this.diasTrab * 300f;
        }

        return pago;
    }
 public float calcularImpuesto(){
     float impuesto = 0.0f;
     float pago = this.calcularPago();
     if (nivel == 1) {
            impuesto = pago * 0.05f;
        }
        if (nivel == 2) {
            impuesto = pago * 0.03f;
        }
         return impuesto;
 }
 public float calcularTotal(){
     float total= 0.0f;
     float pago = this.calcularPago();
     float impuesto = this.calcularImpuesto();
     total= pago - impuesto;
     return total;
 }

}
