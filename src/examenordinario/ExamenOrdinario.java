/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package examenordinario;

/**
 *
 * @author Alexis
 */
public class ExamenOrdinario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        nomina nom = new nomina();
        
        System.out.println("Numero de recibo: " + nom.getNumRecibo());
        System.out.println("Nombre: " + nom.getNombre());
        System.out.println("Puesto: " + nom.getPuesto());
        System.out.println("Nivel: " + nom.getNivel());
        System.out.println("Dias Trabajados: " + nom.getDiasTrab());
        System.out.println("Pago: " + nom.calcularPago());
        System.out.println("Impuesto: " + nom.calcularImpuesto());
        System.out.println("Total a Pagar: " + nom.calcularTotal());
    }
    
}
